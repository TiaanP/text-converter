"""
This program converts English words into SMS-language words and back. It accepts a .txt file as input.
NB: In order to execute this program, the .txt file needs to be placed in the same directory as the program's .py file.
"""
import string, re

vowels = ['a', 'e', 'i', 'o', 'u']
v4 = ''
dict_SMS_Eng = {}                                                       #this dictionary is used to keep track of all SMS-English word pairs
sms_words = []                                                          #this list is used to keep track of all newly converted SMS-words

def cleanWord(word):                                                    #this function converts all letters in a word to lower case, and removes all digits and punctuation
    noDigits = []
    punc = string.punctuation
    no_punc = ''.join([c for c in word.lower() if c not in punc])
    for i in no_punc:
        if not i.isdigit():
            noDigits.append(i)
    return ''.join(noDigits)

def removeVowels(word, vowels):                                         #this function removes all vowels from a word
    letters = []
    for c in word:
        if c not in vowels:
            letters.append(c)
    return ''.join(letters)

def removeRepeats(word):                                                #this function removes all repeating characters and replaces them with a single occurrence of that character
    return re.sub(r'([a-z])\1+', r'\1', word)  

"""
This part of the program converts English words into the appropriate SMS-language version.
"""
user_input1 = raw_input('Enter filename and extension (e.g. file.txt): ')
print '================\n|English to SMS|\n================'

f = open(user_input1, 'r')                                              #opens the specified .txt file and reads its contents
lines = f.readlines()
for i in lines:
    line = i.split(' ')
    for w in line:
        v3 = cleanWord(w)                                               #all digits and punctuation removed
        if len(v3) > 3:                                                 #length is based on the word without digits or punctuation
            v4 = removeVowels(v3, vowels)                               #all vowels are removed
            if v3[0] in vowels:                                         #checks whether a word starts or ends with a vowel, in which case it is added back into the final SMS-word
                v4 = v3[0] + v4
            if v3[len(v3) -1] in vowels:
                v4 = v4 + v3[len(v3) - 1]
            print removeRepeats(v4),                                    #all repeating characters are removed and replaced
            dict_SMS_Eng[v4] = v3
            sms_words.append(v4)
        else:                                                           #if the word's length is <3, the word will stay as it is
            dict_SMS_Eng[v3] = v3
            sms_words.append(v3)
            print v3,

"""
This part of the program converts a given SMS-language word into the appropriate English word.
"""
print '\n----------------------------------'
user_prompt = raw_input('Do you wish to convert to English? ')

if (user_prompt == 'y') or (user_prompt == 'Y') or (user_prompt == 'yes') or (user_prompt == 'Yes'):        #the user is prompted whether they want the SMS-to-English text conversion displayed
    print '\n================\n|SMS to English|\n================'                                          #based on the user's reply either the English words are displayed or the program finishes
    for w in sms_words:
        if w in dict_SMS_Eng:
            print dict_SMS_Eng[w],
else:
    print 'Have a nice day!'
